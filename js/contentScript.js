(() => {
    const header = document.getElementById('header')

    async function requestFromBackground(url, type, signal = null) {
        return new Promise((res, rej) => {
            chrome.runtime.sendMessage({type, url, signal}, response => {
                res(response)
            })
        })
    }

    async function getData() {
        const url = 'https://widget.merkatys.ru/api/v1/main/remains-by-sizes/49008684,36010554,152490541,179140815,189764720,36009417,151383012,94998859,176187528,17539336,135477696,77253449,148868672,160964808,108536114/?order_amount=50000&search_text=платьеженское'
        const response = await requestFromBackground(url, 'json')
        console.log(response)
    }


    const btn = document.createElement('button')
    btn.innerText = 'Click ME'
    btn.addEventListener('click', getData)
    header.prepend(btn)
})()
