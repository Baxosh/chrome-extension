document.getElementById("clickMeButton").addEventListener("click", function () {
    alert('Hello')
    fetch("http://localhost:8000/api/v1/main/requests-count", {
        method: "GET", // or "POST", "PUT", etc.
        headers: {
            "Content-Type": "application/json",
            // You can add additional headers if needed
        },
        // body: JSON.stringify(yourData) // Include this line for POST requests with data
    })
        .then(response => response.json())
        .then(data => {
            alert(JSON.stringify(data))
            // Handle the response data as needed
        })
        .catch(error => console.error("Error:", error));
});

