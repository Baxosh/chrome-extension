chrome.runtime.onMessage.addListener(function (message, sender, senderResponse) {
    if (message.type === "json") {
        fetch(message.url)
            .then(res => res.json())
            .then(response => {
                console.log(response)
                return senderResponse(response)
            })
            .catch(err => senderResponse(err))
    }
    return true
});
